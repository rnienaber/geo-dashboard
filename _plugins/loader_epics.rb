require 'gitlab'
require 'pp'
require 'json'

# CATEGORY_GEO_REPLICATION_VIABLE = Gitlab.epic(1449)
# CATEGORY_GEO_REPLICATION_COMPLETE = Gitlab.epic(1508)
# CATEGORY_DISASTER_RECOVERY = Gitlab.epic(1383)
# CATEGORY_DISASTER_RECOVERY_VIABLE = Gitlab.epic(1507)


class Gitlab::Client
  module Epics

    def epic_issues(epic_iid, group_id)
      get("/groups/#{group_id}/epics/#{epic_iid}/issues", {per_page:100})
    end

    def epic_epics(epic_iid, group_id)
      get("/groups/#{group_id}/epics/#{epic_iid}/epics", {per_page:100})
    end

  end
end

def make_item(name, children)
  pp name
  output = {}
  output["name"] = name


  if children.length > 0
    output["children"] = children
  else
    output["value"] = 1
  end

  output
end

def get_children(epic)

  epics = Gitlab.epic_epics(epic.iid, epic.group_id)
  result = []

  epics.each do |epic|
    result << make_item(epic.title, get_children(epic))
  end

  result
end

def create_epic_json(epic)
  result = make_item(epic.title, get_children(epic))
  result
end


def load_epics
  puts "Loading Epics"

  geo_replication_json = create_epic_json(Gitlab.epic(9970, 1384))
  disaster_recovery_json = create_epic_json(Gitlab.epic(9970, 1383))

  result = {name: "flare", children: [ geo_replication_json, disaster_recovery_json ]}

  write_json("geo_epics.json", result)
end





