require File.expand_path(File.dirname(__FILE__) + '/loader_epics')
require File.expand_path(File.dirname(__FILE__) + '/loader_issues')
require 'json'

def write_json(filename, data)
  File.open("./_data/"+filename,"w") do |f|
    f.write(JSON.pretty_generate(data))
  end
end



Jekyll::Hooks.register :site, :after_init do |s|

  Gitlab.configure do |config|
    config.endpoint       = 'https://gitlab.com/api/v4'
    config.private_token  = ENV["API_PRIVATE_TOKEN"]
  end

  load_epics
  load_issues("12.9")

end