
def load_deliverable_data(milestone)

  issues = []
  current_page = 1

  loop do

    api_results = Gitlab.issues(nil, {scope:"all", labels:"Deliverable, group::geo", milestone: milestone, page:current_page, per_page:20})
    break if api_results.length == 0

    issues.concat(api_results)
    current_page += 1
  end

  # pp issues.group_by { |i| i['epic_iid'] }.keys

  # require 'pry'

  nil_epic = {id: nil, title: 'Other'}

  epic_id_issues = issues.map(&:to_h).group_by { |i| i['epic_iid'] }
  epics_and_issues = epic_id_issues.values.reduce([]) do |acc, issues|
    epic = issues[0]['epic'] || nil_epic
    epic['issues'] = issues

    issues.each { |i| i.delete('epic') }
    acc.push(epic)
  end

  write_json("deliverable_issues.json", epics_and_issues)
end


def load_issues(milestone)

  puts "Loading Issues"

  load_deliverable_data(milestone)

end