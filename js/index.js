import initChart from './chart.js';

const domHook = document.getElementById('chart');

if (domHook) {
  const chartNode = initChart();
  domHook.appendChild(chartNode);
} else {
  console.log('Cannot find element with ID \'chart\'');
}