export default {
  "name": "flare",
  "children": [
    {
      "name": "🌏 Geo-replication",
      "children": [
        {
          "name": "🌏 Viable maturity for Geo-replication",
          "children": [
            {
              "name": "Geo: Support Geo on Kubernetes",
              "value": 1
            }
          ]
        },
        {
          "name": "🌏 Complete maturity for Geo-replication",
          "children": [
            {
              "name": "Geo:  Simplify Geo upgrade process",
              "children": [
                {
                  "name": "Geo Upgrade Demos",
                  "value": 1
                },
                {
                  "name": "Geo: Improved Geo upgrade documentation",
                  "value": 1
                },
                {
                  "name": "Geo: More robust Foreign Data Tables updates",
                  "value": 1
                },
                {
                  "name": "Test no-downtime HA upgrades (including Geo)",
                  "value": 1
                }
              ]
            },
            {
              "name": "Geo: Verify all replicated data",
              "children": [
                {
                  "name": "Geo: Repository checking with `git fsck`",
                  "value": 1
                }
              ]
            },
            {
              "name": "Geo: Geo should be simple to install",
              "children": [
                {
                  "name": "Service Discovery solution for a GitLab Cluster",
                  "value": 1
                }
              ]
            },
            {
              "name": "Geo: Improve Administrator Experience",
              "children": [
                {
                  "name": "Geo: Simplify processes for installation and upgrades",
                  "value": 1
                },
                {
                  "name": "Geo: Omnibus Improvements",
                  "children": [
                    {
                      "name": "Geo: Omnibus Improvements regarding FDW",
                      "value": 1
                    }
                  ]
                },
                {
                  "name": "Geo: Improve UI/UX on Administrator Panel",
                  "value": 1
                },
                {
                  "name": "Geo: Resolve Setup Problems and Improve Documentation",
                  "value": 1
                },
                {
                  "name": "Geo: Assistance in Debugging Setup/Configuration Problems",
                  "value": 1
                }
              ]
            },
            {
              "name": "Geo: Automatically choose the Geo node for the best user experience",
              "children": [
                {
                  "name": "Geo supports using a single, geolocation-aware Git URL",
                  "value": 1
                }
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "🚨 Disaster Recovery",
      "children": [
        {
          "name": "🚨Disaster Recovery viable maturity",
          "children": [
            {
              "name": "Geo: Support multiple database nodes in the Secondary",
              "value": 1
            },
            {
              "name": "Geo: Improved support for a planned failover",
              "children": [
                {
                  "name": "Geo: Discovery  of current planned failover process",
                  "value": 1
                },
                {
                  "name": "Geo: Replication should be easy to pause and resume",
                  "value": 1
                },
                {
                  "name": "Geo: Create a maintenance mode / read-only mode",
                  "value": 1
                },
                {
                  "name": "Geo: Simplify planned failover documentation",
                  "value": 1
                },
                {
                  "name": "Geo: Simplify the demotion process",
                  "value": 1
                },
                {
                  "name": "Geo: A demoted primary should not resync all data",
                  "value": 1
                }
              ]
            },
            {
              "name": "Integrate Geo into DR Procedures for GitLab",
              "children": [
                {
                  "name": "Define a DR plan for Gitlab.com using Geo in different phases",
                  "value": 1
                },
                {
                  "name": "Test Geo DR strategy on staging.gitlab.com",
                  "value": 1
                }
              ]
            },
            {
              "name": "Geo: Promotion, Demotion and Failover",
              "value": 1
            },
            {
              "name": "Geo: Add Unreplicated Data Types",
              "children": [
                {
                  "name": "Geo support for Maven repositories",
                  "value": 1
                },
                {
                  "name": "Geo: Support custom hooks",
                  "value": 1
                },
                {
                  "name": "Geo: Support GitLab Pages",
                  "value": 1
                }
              ]
            }
          ]
        }
      ]
    }
  ]
}